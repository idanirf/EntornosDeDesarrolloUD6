
--- 
# Unidad 6 - Diseño Orientado a Objetos: Diagramas de Clases
<img src=daniimg.png>



## Teoría
- Con las anotaciones tomadas en clase, he construido un documento con los siguientes puntos:
    * 
    * 
    * 
    * 
    


## Prácticas
- Biblioteca UML.




## Contacto
* **Contacto:** daniel.rodriguezfernandez@alumno.iesluisvives.org
* **Twiter:** [@idanirf](https://twitter.com/idanirf)
* **Linkedin:** [Visita mi perfil](https://www.linkedin.com/in/danielrodriguezfernandez03002/)

[![Twitter](https://img.shields.io/twitter/follow/idanirf?style=social)](https://twitter.com/idanirf)
[![GitHub](https://img.shields.io/github/followers/idanirf?style=social)](https://github.com/idanirf)

---